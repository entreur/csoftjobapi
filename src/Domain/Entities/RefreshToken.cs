﻿namespace Domain.Entities
{
    public class RefreshToken : BaseEntitiy
    {
        public string Token { get; set; }
        public DateTime Expires { get; set; }
    }
}
