﻿using Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    /// <summary>
    /// Represents Administrative Users
    /// </summary>
    public class User : BaseEntitiy
    {
        [Required]
        public string Fullname { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [EmailAddress] 
        public string Email { get; set; }
        [Required]
        public Role UserRole { get; set; } = Role.NONE;
        public string ImageUrl { get; set; }
        public string RefreshToken { get; set; }
        public DateTime TokenCreated { get; set; }
        public DateTime TokenExpires { get; set; }
    }
}
