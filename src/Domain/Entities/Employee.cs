﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Employee : BaseEntitiy
    {
        [Required]
        public string Fullname { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public EmployeeRole Role { get; set; }
        [Required]
        public float Salary { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        public string ProfilePicture { get; set; } = string.Empty;
    }
}
