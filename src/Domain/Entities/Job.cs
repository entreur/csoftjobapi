﻿using Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Job : BaseEntitiy
    {
        [Required]
        [MaxLength(250)]
        [MinLength(5)]
        public string Title { get; set; }
        [Required]
        public string Employer { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Description { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Requirements { get; set; }
        [Required]
        public DateTime ExpirationDate { get; set; }
        #region Enums
        [Required]
        public EmploymentType Type { get; set; }
        [Required]
        public Locations Location { get; set; }
        [Required]
        public EducationLevel Education { get; set; }
        #endregion
        [Required]
        public Category Category { get; set; }
        [Required]
        public User Author { get; set; }
    }
}
