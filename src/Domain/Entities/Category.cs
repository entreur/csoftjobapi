﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Category:BaseEntitiy
    {
        [Required]
        [MaxLength(250,ErrorMessage = "Cannot exceed 250")]
        public string Title { get; set; }
    }
}
