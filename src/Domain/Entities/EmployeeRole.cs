﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class EmployeeRole : BaseEntitiy
    {
        [Required]
        public string Name { get; set; }
    }
}
