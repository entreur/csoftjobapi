﻿namespace Domain.Enums
{
    public enum Locations
    {
        BAKU = 1,
        LANKARAN = 2,
        MASALLI = 3,
        QUBA = 4,
        GANJA = 5,
    }
}
