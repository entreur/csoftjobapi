﻿namespace Domain.Enums
{
    public enum Role
    {
        ADMIN = 1,
        MODERATOR = 2,
        MANAGER = 3,
        HR = 4,
        NONE = 5,   
    }
}
