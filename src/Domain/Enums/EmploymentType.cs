﻿namespace Domain.Enums
{
    public enum EmploymentType
    {
        FULLTIME = 1,
        PARTTIME = 2,
        FREELANCE = 3,
        INTERNSHIP = 4,
    }
}
