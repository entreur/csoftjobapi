﻿namespace Domain.Enums
{
    public enum EducationLevel
    {
        BACHELORS = 1,
        MASTERS = 2,
        HIGHSCHOOL = 3,
        NOEDUCATION = 4,
    }
}
