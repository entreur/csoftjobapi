﻿using Application.DataTransferObjects;
using Application.Services.EmployeeService.Role;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController(IEmployeeRoleService service) : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<List<EmployeeRole>>> GetAllRoles()
        {
            return await service.GetAllRoles();
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<EmployeeRole>> GetRoleById(Guid id)
        {
           return await service.GetAllRoleById(id);
        }
        [HttpPost]
        public async Task<ActionResult<EmployeeRole>> CreateRole([FromForm] EmployeeRoleDTO request)
        {
            return await service.CreateRole(request);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> DeleteRole(Guid id)
        {
            if(await service.DeleteRole(id))
            {
                return Ok($"{id} has been deleted");
            }
            return BadRequest($"{id} could not be found");
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<EmployeeRole>> UpdateRole(Guid id,[FromForm] EmployeeRoleDTO request)
        {
            try
            {
                return Ok(await service.UpdateRole(id, request));
            }
            catch(Exception ex) 
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
