﻿using Application.DataTransferObjects;
using Application.Services.JobService;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobController(IJobService service) : ControllerBase
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult<List<Job>> GetJobs()
        {
            return Ok(service.GetAllJobs());
        }
        [HttpGet("{id}")]
        [AllowAnonymous]
        public IActionResult GetJobById(Guid id)
        {
            return Ok(service.GetJobById(id));
        }
        [HttpGet]
        [Route("/api/Job/bycategory/{id}")]
        [AllowAnonymous]
        public IActionResult GetJobByCategoryId(Guid id)
        {
            return Ok(service.GetJobByCategoryId(id));  
        }
        [HttpPost]
        [Authorize(Roles = "HR")]
        public IActionResult PostJob([FromForm] JobDTO job)
        {
            return Ok(service.CreateJob(job));
        }
        [HttpDelete("{id}")]
        [Authorize(Roles = "HR")]
        public IActionResult DeleteJob(Guid id)
        {
            if (!service.DeleteJob(id))
            {
                return BadRequest($"Job with {id} could not be found.");
            }
            return Ok($"Job - {id} Removed succesfully");
        }
        [HttpPut("{id}")]
        [Authorize(Roles = "HR")]
        public IActionResult EditJob(Guid id, [FromForm] JobDTO data)
        {
            try
            {
                service.UpdateJob(id, data);
            }
            catch
            {
                return BadRequest($"Job - {id} could not be found");
            }
            return Ok();
        }
    }
}
