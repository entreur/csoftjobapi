﻿using Application.DataTransferObjects;
using Application.Repositories;
using Application.Services.EmployeeService;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController(IEmployeeService service) : ControllerBase
    {
        [HttpGet("{id}")]
        [Authorize(Roles = "HR")]
        public async Task<ActionResult<Employee>> GetEmployeeById(Guid id)
        {
            return Ok(await service.GetEmployeeById(id));
        }

        [HttpGet(), Route("/api/employee/getbyemail/{email}")]
        [Authorize(Roles = "HR")]
        public async Task<ActionResult<Employee>> GetEmployeeByEmail(string email)
        {
            return Ok(await service.GetEmployeeByEmail(email));
        }
        [HttpGet]
        public async Task<ActionResult<List<Employee>>> GetAllEmployees()
        {
            return Ok(await service.GetAllEmployees());
        }
        [HttpPost]
        [Authorize(Roles = "HR")]
        public async Task<ActionResult<Employee>> CreateEmployee(EmployeeDTO request)
        {
            try
            {
                return Ok(await service.CreateEmployee(request));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpDelete("{id}")]
        [Authorize(Roles = "HR")]
        public async Task<ActionResult> DeleteEmployee(Guid id)
        {
            if(await service.DeleteEmployee(id))
            {
                return Ok($"{id} - Employee deleted");
            }
            return BadRequest($"{id} - Could not be found");
        }

        [HttpPut]
        [Authorize(Roles = "HR")]
        public async Task<ActionResult<Employee>> UpdateEmployee(Guid id, EmployeeDTO request)
        {
            try
            {
                return Ok(await service.UpdateEmployee(id,request));
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
