﻿using Application.DataTransferObjects;
using Application.DataTransferObjects.Authentication_DTO;
using Application.Services.AuthenticationService;
using Application.Services.TokenService;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController(ITokenService tokenService,IAuthenticationService service): ControllerBase
    {
        [HttpPost]
        [Route("/api/Register")]
        public async Task<ActionResult<User>> Register([FromForm] RegisterDTO request)
        {
            if (!request.Password.Equals(request.PasswordConfirm))
            {
                return BadRequest("Password and confirm password doesn't match");
            }
            try
            {
                User? result = await service.Register(request);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("/api/Login")]
        public async Task<ActionResult<ResponseDTO>> Login([FromForm] LoginDTO request)
        {
            string? token = await service.Login(request);
            RefreshToken? newRefreshToken = GenerateRefreshToken();
            SetRefreshToken(newRefreshToken, request.Email);
            return Ok(new ResponseDTO
            {
                JWToken = token,
                refreshToken = newRefreshToken.Token,
            });
        }

        [HttpPost]
        [Route("/api/Logout")]
        public async Task<ActionResult<string>> Logout()
        {
            Request.Cookies.TryGetValue("refreshToken", out var token);
            if (token.Equals(string.Empty))
            {
                return BadRequest("Not logged in.");
            }
            Response.Cookies.Delete("refreshToken");
            return Ok(await service.Logout(token));
        }

        private static RefreshToken GenerateRefreshToken()
        {
            var refreshToken = new RefreshToken
            {
                Token = Convert.ToBase64String(RandomNumberGenerator.GetBytes(64)),
                Expires = DateTime.UtcNow.AddDays(2),
                DateCreated = DateTime.UtcNow
            };

            return refreshToken;
        }

        private void SetRefreshToken(RefreshToken newRefreshToken, string email)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Secure = true,
                Expires = newRefreshToken.Expires
            };
            Response.Cookies.Delete("refreshToken");
            Response.Cookies.Append("refreshToken", newRefreshToken.Token, cookieOptions);
            tokenService.InsertToken(newRefreshToken, email);
        }
    }
}
