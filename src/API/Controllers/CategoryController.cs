﻿using Application.DataTransferObjects;
using Application.Services.CategoryService;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController(ICategoryService service) : ControllerBase
    {
        [HttpGet]
        public ActionResult<List<Category>> GetAllCategories()
        {
            return Ok(service.GetAllCategories());
        }
        [HttpGet("{id}")]
        public IActionResult GetCategoryById(Guid id)
        {
            return Ok(service.GetCategoryById(id));
        }
        [HttpPost]
        [Authorize(Roles = "HR")]
        public IActionResult CreateCategory([FromForm] CategoryDTO data)
        {
            return Ok(service.CreateCategory(data));
        }
        [HttpPut("{id}")]
        [Authorize(Roles = "HR")]
        public IActionResult UpdateCategory(Guid id, [FromForm] CategoryDTO data)
        {
            try
            {
                service.UpdateCategory(id, data);
            }
            catch
            {
                return BadRequest($"Job - {id} could not be found");
            }
            return Ok();
        }
        [HttpDelete("{id}")]
        [Authorize(Roles = "HR")]
        public IActionResult DeleteCategory(Guid id)
        {
            if (!service.DeleteCategory(id))
            {
                return BadRequest($"Category with {id} could not be found.");
            }
            return Ok($"Category - {id} Removed succesfully");
        }
    }
}
