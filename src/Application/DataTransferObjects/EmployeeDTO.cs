﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Application.DataTransferObjects
{
    public class EmployeeDTO
    {
        [Required]
        public string Fullname { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public Guid RoleId { get; set; }
        [Required]
        public float Salary { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        public IFormFile ProfilePicture { get; set; }
    }
}
