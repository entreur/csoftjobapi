﻿using System.ComponentModel.DataAnnotations;

namespace Application.DataTransferObjects
{
    public class CategoryDTO
    {
        [Required]
        [MaxLength(150)]
        public string Title { get; set; }
    }
}
