﻿using System.ComponentModel.DataAnnotations;

namespace Application.DataTransferObjects.Authentication_DTO
{
    public class ChangePasswordDTO
    {
        [Required]
        public string CurrentPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required]
        public string Token { get; set; }
    }
}
