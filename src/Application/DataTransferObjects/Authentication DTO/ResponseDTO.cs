﻿using System.ComponentModel.DataAnnotations;

namespace Application.DataTransferObjects.Authentication_DTO
{
    public class ResponseDTO
    {
        [Required]
        public string JWToken { get; set; }
        [Required]
        public string refreshToken { get; set; }
    }
}
