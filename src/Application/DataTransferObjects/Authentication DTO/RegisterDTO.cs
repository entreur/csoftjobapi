﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Application.DataTransferObjects
{
    public class RegisterDTO
    {
        [Required]
        public string Fullname { get; set; } = string.Empty;
        [EmailAddress]
        public string Email { get; set; } = string.Empty;
        [Required]
        [PasswordPropertyText]
        public string Password { get; set; } = string.Empty;
        [Required]
        [PasswordPropertyText]
        public string PasswordConfirm { get; set; } = string.Empty;
        [DataType(DataType.Upload)]
        public IFormFile ProfilePicture { get; set; }
    }
}
