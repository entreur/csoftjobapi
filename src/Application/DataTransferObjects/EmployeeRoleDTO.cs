﻿using System.ComponentModel.DataAnnotations;

namespace Application.DataTransferObjects
{
    public class EmployeeRoleDTO
    {
        [Required]
        public string Name { get; set; }
    }
}
