﻿using Application.DataTransferObjects;
using Application.Repositories;
using Domain.Entities;

namespace Application.Services.JobService
{
    public class JobService(IJobRepository repo) : IJobService
    {

        public List<Job> GetAllJobs()
        {
            return repo.GetAllJobs();
        }

        public List<Job> GetJobByCategoryId(Guid id)
        {
            return repo.GetJobByCategoryId(id);
        }

        public Job? GetJobById(Guid id)
        {
            return repo.GetJobById(id);
        }

        public Job CreateJob(JobDTO data)
        {
            return repo.CreateJob(data);
        }

        public bool DeleteJob(Guid id)
        {
            return repo.DeleteJob(id);
        }

        public Job? UpdateJob(Guid id, JobDTO data)
        {
            return repo.UpdateJob(id, data);
        }
    }
}
