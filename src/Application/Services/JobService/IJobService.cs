﻿using Application.DataTransferObjects;
using Domain.Entities;

namespace Application.Services.JobService
{
    public interface IJobService
    {
        List<Job> GetAllJobs();
        List<Job> GetJobByCategoryId(Guid id);
        Job? GetJobById(Guid id);
        Job CreateJob(JobDTO data);
        bool DeleteJob(Guid id);
        Job? UpdateJob(Guid id, JobDTO data);
    }
}
