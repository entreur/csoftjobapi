﻿using Application.DataTransferObjects;
using Application.Repositories;
using Domain.Entities;

namespace Application.Services.EmployeeService
{
    public class EmployeeService(IEmployeeRepository repo) : IEmployeeService
    {
        public async Task<Employee> CreateEmployee(EmployeeDTO request)
        {
            return await repo.CreateEmployee(request);
        }

        public async Task<bool> DeleteEmployee(Guid id)
        {
            return await repo.DeleteEmployee(id);
        }

        public async Task<List<Employee>> GetAllEmployees()
        {
            return await repo.GetAllEmployees();
        }

        public async Task<Employee> GetEmployeeByEmail(string email)
        {
            return await repo.GetEmployeeByEmail(email);
        }

        public async Task<Employee> GetEmployeeById(Guid id)
        {
            return await repo.GetEmployeeById(id);   
        }

        public async Task<Employee> UpdateEmployee(Guid id,EmployeeDTO request)
        {
            return await repo.UpdateEmployee(id,request);
        }
    }
}
