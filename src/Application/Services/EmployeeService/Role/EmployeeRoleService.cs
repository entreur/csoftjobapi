﻿using Application.DataTransferObjects;
using Application.Repositories;
using Domain.Entities;

namespace Application.Services.EmployeeService.Role
{
    public class EmployeeRoleService(IEmployeeRoleRepository repo) : IEmployeeRoleService
    {
        public async Task<EmployeeRole> CreateRole(EmployeeRoleDTO request)
        {
            return await repo.CreateRole(request);
        }

        public async Task<bool> DeleteRole(Guid id)
        {
            return await repo.DeleteRole(id);
        }

        public Task<EmployeeRole> GetAllRoleById(Guid id)
        {
            return repo.GetAllRoleById(id);
        }

        public async Task<List<EmployeeRole>> GetAllRoles()
        {
           return await repo.GetAllRoles();
        }

        public async Task<EmployeeRole> UpdateRole(Guid id, EmployeeRoleDTO request)
        {
            return await repo.UpdateRole(id, request);
        }
    }
}
