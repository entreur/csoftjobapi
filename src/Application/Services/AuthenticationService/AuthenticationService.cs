﻿using Application.DataTransferObjects;
using Application.DataTransferObjects.Authentication_DTO;
using Application.Repositories;
using Domain.Entities;

namespace Application.Services.AuthenticationService
{
    public class AuthenticationService(IAuthenticationRepository repo) : IAuthenticationService
    {
        public Task<string> ChangePassword(ChangePasswordDTO request)
        {
            throw new NotImplementedException();
        }

        public async Task<string> Login(LoginDTO request)
        {
            return await repo.Login(request);
        }

        public async Task<string> Logout(string email)
        {
            return await repo.Logout(email);
        }

        public async Task<User> Register(RegisterDTO request)
        {
            return await repo.Register(request);
        }
    }
}
