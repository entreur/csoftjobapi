﻿using Application.DataTransferObjects;
using Application.DataTransferObjects.Authentication_DTO;
using Domain.Entities;

namespace Application.Services.AuthenticationService
{
    public interface IAuthenticationService
    {
        public Task<string> Login(LoginDTO request);
        public Task<string> Logout(string email);
        public Task<User> Register(RegisterDTO request);
        public Task<string> ChangePassword(ChangePasswordDTO request);
    }
}
