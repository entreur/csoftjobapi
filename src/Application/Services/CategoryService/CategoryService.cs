﻿using Application.DataTransferObjects;
using Application.Repositories;
using Domain.Entities;

namespace Application.Services.CategoryService
{
    public class CategoryService(ICategoryRepository repo) : ICategoryService
    {
        public Category CreateCategory(CategoryDTO data)
        {
            return repo.CreateCategory(data);
        }

        public bool DeleteCategory(Guid id)
        {
            return repo.DeleteCategory(id);
        }

        public List<Category> GetAllCategories()
        {
            return repo.GetAllCategories();
        }

        public Category? GetCategoryById(Guid id)
        {
            return repo.GetCategoryById(id);
        }

        public Category? UpdateCategory(Guid id, CategoryDTO data)
        {
            return repo.UpdateCategory(id, data);
        }
    }
}
