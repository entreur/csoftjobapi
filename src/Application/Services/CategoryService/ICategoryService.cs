﻿using Application.DataTransferObjects;
using Domain.Entities;

namespace Application.Services.CategoryService
{
    public interface ICategoryService
    {
        List<Category> GetAllCategories();
        Category? GetCategoryById(Guid id);
        Category CreateCategory(CategoryDTO data);
        bool DeleteCategory(Guid id);
        Category? UpdateCategory(Guid id, CategoryDTO data);
    }
}
