﻿using Domain.Entities;

namespace Application.Services.TokenService
{
    public interface ITokenService
    {
        Task<string> InsertToken(RefreshToken token,string email);
    }
}
