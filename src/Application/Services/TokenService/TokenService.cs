﻿using Application.Repositories;
using Domain.Entities;

namespace Application.Services.TokenService
{
    public class TokenService(ITokenRepository repo) : ITokenService
    {
        public async Task<string> InsertToken(RefreshToken token,string email)
        {
            return await repo.InsertToken(token,email);
        }
    }
}
