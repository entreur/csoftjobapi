﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface ITokenRepository
    {
        public Task<string> InsertToken(RefreshToken token,string email);
    }
}
