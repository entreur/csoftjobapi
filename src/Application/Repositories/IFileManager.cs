﻿using Microsoft.AspNetCore.Http;

namespace Application.Repositories
{
    public interface IFileManager
    {
        Task<string> Upload(IFormFile file, string _PATH);
        bool Delete(string fileName, string _PATH);
        bool FileExists(string fileName, string _PATH);
    }
}
