﻿using Application.DataTransferObjects;
using Domain.Entities;

namespace Application.Repositories
{
    public interface ICategoryRepository
    {
        List<Category> GetAllCategories();
        Category? GetCategoryById(Guid id);
        Category CreateCategory(CategoryDTO data);
        bool DeleteCategory(Guid id);
        Category? UpdateCategory(Guid id, CategoryDTO data);
    }
}
