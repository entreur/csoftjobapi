﻿using Application.DataTransferObjects;
using Domain.Entities;

namespace Application.Repositories
{
    public interface IEmployeeRepository
    {
        public Task<List<Employee>> GetAllEmployees();
        public Task<Employee> GetEmployeeById(Guid id);
        public Task<Employee> GetEmployeeByEmail(string email);
        public Task<Employee> CreateEmployee(EmployeeDTO request);
        public Task<Employee> UpdateEmployee(Guid id,EmployeeDTO request);
        public Task<bool> DeleteEmployee(Guid id);
    }
}
