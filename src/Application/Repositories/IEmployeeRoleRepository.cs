﻿using Application.DataTransferObjects;
using Domain.Entities;

namespace Application.Repositories
{
    public interface IEmployeeRoleRepository
    {
        Task<bool> DeleteRole(Guid id);
        Task<EmployeeRole> CreateRole(EmployeeRoleDTO request);
        Task<EmployeeRole> UpdateRole(Guid id, EmployeeRoleDTO request);
        Task<List<EmployeeRole>> GetAllRoles();
        Task<EmployeeRole> GetAllRoleById(Guid id);
    }
}
