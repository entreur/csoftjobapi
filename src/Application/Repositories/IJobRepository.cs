﻿using Application.DataTransferObjects;
using Domain.Entities;

namespace Application.Repositories
{
    public interface IJobRepository
    {
        List<Job> GetAllJobs();

        List<Job> GetJobByCategoryId(Guid id);

        Job? GetJobById(Guid id);

        Job CreateJob(JobDTO data);

        Job? UpdateJob(Guid id,JobDTO data);

        bool DeleteJob(Guid id);
    }
}
