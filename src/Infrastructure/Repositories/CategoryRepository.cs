﻿using Application.DataTransferObjects;
using Application.Repositories;
using Domain.Entities;
using Infrastructure.Data;

namespace Infrastructure
{
    public class CategoryRepository(ApplicationDbContext context) : ICategoryRepository
    {
        public List<Category> GetAllCategories()
        {
           return context.Categories.ToList();
        }

        public Category? GetCategoryById(Guid id)
        {
            return context.Categories.Find(id);
        }

        public Category CreateCategory(CategoryDTO data)
        {
            Category category = new()
            {
                Title = data.Title,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow, 
            };
            context.Categories.Add(category);
            context.SaveChanges();
            return category;
        }

        public Category? UpdateCategory(Guid id, CategoryDTO data)
        {
            Category? category = context.Categories.Find(id);
            if (category.Equals(null))
            {
                throw new Exception($"Category - {id} Cannot be found");
            }
            #region Data Binding
            category.Title = data.Title;
            category.DateUpdated = DateTime.UtcNow;
            #endregion
            context.Categories.Update(category);
            context.SaveChanges();
            return category;
        }

        public bool DeleteCategory(Guid id)
        {
            Category? data = context.Categories.Find(id);
            if (!data.Equals(null))
            {
                context.Categories.Remove(data);
                context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
