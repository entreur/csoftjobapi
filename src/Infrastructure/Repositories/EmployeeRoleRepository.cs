﻿using Application.DataTransferObjects;
using Application.Repositories;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class EmployeeRoleRepository(ApplicationDbContext context) : IEmployeeRoleRepository
    {

        public async Task<EmployeeRole> CreateRole(EmployeeRoleDTO request)
        {
            EmployeeRole data = new()
            {
                Name = request.Name,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow,
            };
            context.EmployeeRoles.Add(data);
            await context.SaveChangesAsync();
            return data;
        }

        public async Task<bool> DeleteRole(Guid id)
        {
            var data = await context.EmployeeRoles.Where(r => r.Id.Equals(id)).FirstOrDefaultAsync();
            if (data.Equals(null))
            {
                return false;
            }
            context.EmployeeRoles.Remove(data);
            await context.SaveChangesAsync();
            return true;
        }

        public async Task<EmployeeRole> GetAllRoleById(Guid id)
        {
            return await context.EmployeeRoles.Where(r => r.Id == id).FirstOrDefaultAsync();
        }

        public async Task<List<EmployeeRole>> GetAllRoles()
        {
            return await context.EmployeeRoles.ToListAsync();
        }

        public async Task<EmployeeRole> UpdateRole(Guid id, EmployeeRoleDTO request)
        {
            var data = await context.EmployeeRoles.Where(r => r.Id.Equals(id)).FirstAsync();
            if (data.Equals(null))
            {
                throw new Exception($"{id} - Could not be found");
            }
            data.Name = request.Name;
            data.DateUpdated = DateTime.UtcNow;
            context.Update(data);
            await context.SaveChangesAsync();
            return data;
        }
    }
}
