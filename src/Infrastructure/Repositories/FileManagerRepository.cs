﻿using Application.Repositories;
using Infrastructure.Utilities;
using Microsoft.AspNetCore.Http;

namespace Infrastructure
{
    public class FileManager() : IFileManager
    {
        private const string _ROOT = "Uploads";

        public bool Delete(string fileName, string _PATH)
        {
            string path = $"{GetWritePath(_PATH)}/{fileName}";
            if (File.Exists(path))
            {
                File.Delete(path);
                return true;
            }
            return false;
        }

        public bool FileExists(string fileName, string _PATH)
        {
            return File.Exists(Path.Combine(Directory.GetCurrentDirectory(), _ROOT, _PATH, fileName));
        }

        async Task<string> IFileManager.Upload(IFormFile file, string _PATH)
        {
            if (file.Equals(null) || !ImageUtilities.IsImageFile(file))
            {
                throw new Exception("File does not exist or is an unsupported format.");
            }
            string writePath = GetWritePath(_PATH);
            string newName = GenerateUniqueFileName(file);
            GenerateFolder(path: writePath);
            using FileStream stream = new(Path.Combine(writePath, newName), FileMode.Create);
            await file.CopyToAsync(stream);
            return $"{_ROOT}/{_PATH}/{newName}";
        }

        private static void GenerateFolder(string path)
        {
            Directory.CreateDirectory(path);
        }
        private static string GenerateUniqueFileName(IFormFile file)
        {
            string extension = file.FileName.Split('.').Last();
            return $"{Guid.NewGuid()}.{extension}";
        }
        private static string GetWritePath(string path)
        {
            return Path.Combine(Directory.GetCurrentDirectory(), _ROOT, path);
        }
    }
}
