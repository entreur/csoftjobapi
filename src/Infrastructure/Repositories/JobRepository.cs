﻿using Application.DataTransferObjects;
using Application.Repositories;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class JobRepository(ApplicationDbContext context) : IJobRepository
    {
        public List<Job> GetAllJobs()
        {
            return context.Jobs
                .Include(j => j.Category)
                .ToList();
        }

        public List<Job> GetJobByCategoryId(Guid id)
        {
            return context.Jobs
                .Where(j => j.Category.Id == id)
                .ToList();
        }

        public Job? GetJobById(Guid id)
        {
            return context.Jobs
                .Where(j => j.Id == id)
                .Include(j => j.Category)
                .First();
        }

        public Job CreateJob(JobDTO data)
        {
            #region Data Binding
            Job job = new()
            {
                Title = data.Title,
                Requirements = data.Requirements,
                Type = data.Type,
                Education = data.Education,
                Employer = data.Employer,
                Location = data.Location,
                Description = data.Description,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow,
                ExpirationDate = data.ExpirationDate,
                Category = context.Categories.Find(data.CategoryId)
            };
            #endregion
            context.Jobs.Add(job);
            context.SaveChanges();
            return job;
        }

        public bool DeleteJob(Guid id)
        {
            Job? data = context.Jobs.Find(id);
            if (!data.Equals(null))
            {
                context.Jobs.Remove(data);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public Job? UpdateJob(Guid id, JobDTO data)
        {
            Job? job = context.Jobs.Find(id);
            if(job.Equals(null)) 
            {
                throw new Exception($"Job - {id} Cannot be found");
            }
            #region Data Binding
            job.Title = data.Title;
            job.Description = data.Description;
            job.Requirements = data.Requirements;
            job.Location = data.Location;
            job.Type = data.Type;
            job.DateUpdated = DateTime.UtcNow;
            #endregion
            context.Jobs.Update(job);
            context.SaveChanges();
            return job;
        }
    }
}
