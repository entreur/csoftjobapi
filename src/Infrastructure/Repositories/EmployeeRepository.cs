﻿using Application.DataTransferObjects;
using Application.Repositories;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class EmployeeRepository(ApplicationDbContext context, IFileManager fileManager) : IEmployeeRepository
    {
        private const string _PATH = "Employee Pictures";
        public async Task<Employee> CreateEmployee(EmployeeDTO request)
        {
            if (await context.Users.AnyAsync(u => u.Email == request.Email))
            {
                throw new Exception($"{request.Email} has already been registered.");
            }
            #region Data Binding
            var employee = new Employee()
            {
                Fullname = request.Fullname,
                Email = request.Email,
                ProfilePicture = await fileManager.Upload(request.ProfilePicture, _PATH),
                Role = context.EmployeeRoles.Where(r => r.Id == request.RoleId).FirstOrDefault(),  
                BirthDate = request.BirthDate,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow,
            };
            #endregion
            await context.AddAsync(employee);
            await context.SaveChangesAsync();
            return employee;
        }

        public async Task<bool> DeleteEmployee(Guid id)
        {
            var data = await context.Employees.Where(e => e.Id.Equals(id)).FirstOrDefaultAsync();
            if (data.Equals(null))
            {
                return false;
            }
            context.Employees.Remove(data);
            await context.SaveChangesAsync();
            return true;
        }

        public async Task<List<Employee>> GetAllEmployees()
        {
            return await context.Employees
                .Include(e => e.Role)
                .ToListAsync();
        }

        public async Task<Employee> GetEmployeeByEmail(string email)
        {
            return await context.Employees
                .Where(e => e.Email.Equals(email))
                .Include(e => e.Role)
                .FirstAsync();
        }

        public async Task<Employee> GetEmployeeById(Guid id)
        {
            return await context.Employees
                .Where(e => e.Id == id)
                .Include(e => e.Role)
                .FirstOrDefaultAsync();
        }

        public async Task<Employee> UpdateEmployee(Guid id,EmployeeDTO request)
        {
            var data = context.Employees.Where(e => e.Id.Equals(id)).FirstOrDefault();
            if (data.Equals(null))
            {
                throw new Exception("User not found.");
            }
            #region Data Binding
            data.Fullname = request.Fullname;
            data.Email = request.Email;
            data.Salary = request.Salary;
            data.Role = context.EmployeeRoles.Where(r => r.Id == request.RoleId).First();
            if (!request.ProfilePicture.Equals(null))
            {
                fileManager.Delete(data.ProfilePicture,_PATH);
                data.ProfilePicture = await fileManager.Upload(request.ProfilePicture, _PATH);
            }
            data.BirthDate = request.BirthDate;
            data.DateUpdated = DateTime.UtcNow;
            #endregion
            context.Employees.Update(data);
            context.SaveChanges();
            return data;
        }
    }
}
