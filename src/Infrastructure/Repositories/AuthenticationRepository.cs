﻿using Application.DataTransferObjects;
using Application.DataTransferObjects.Authentication_DTO;
using Application.Repositories;
using CryptoHelper;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Infrastructure
{
    public class AuthenticationRepository(ApplicationDbContext context, IConfiguration configuration, IFileManager fileManager) : IAuthenticationRepository
    {
        private const string _PATH = "Profile Pictures";
        public async Task<string> Login(LoginDTO request)
        {
            var user = await context.Users.Where(u=> u.Email.Equals(request.Email)).FirstAsync();

            if (user.Email != request.Email)
            {
                throw new Exception($"User by the email {request.Email} not found");
            }

            if (!Crypto.VerifyHashedPassword(user.Password,request.Password))
            {
                throw new Exception("Password do not match");
            }

            string token = CreateToken(user);
            return token;
        }

        public async Task<User> Register(RegisterDTO request)
        {
            if (await context.Users.AnyAsync(u => u.Email == request.Email))
            {
                throw new Exception($"{request.Email} has already been registered. Forgot Password?");
            }
            #region Data Binding
            User user = new()
            {
                Email = request.Email,
                Fullname = request.Fullname,
                Password = Crypto.HashPassword(request.Password),
                UserRole = Domain.Enums.Role.NONE,
                ImageUrl = await fileManager.Upload(request.ProfilePicture, _PATH),
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow,
            };
            await context.Users.AddAsync(user);
            await context.SaveChangesAsync();
            #endregion
            return user;
        }

        public void InsertRefreshToken(RefreshToken refreshToken,User user)
        {
            user.RefreshToken = refreshToken.Token;
            user.TokenExpires = refreshToken.Expires;
            user.TokenCreated = refreshToken.DateCreated;
            context.Update(user);
            context.SaveChanges();
        }

        private string CreateToken(User user)
        {
            List<Claim> claims = new()
            {
                new Claim(ClaimTypes.Name,user.Fullname),
                new Claim(ClaimTypes.Role, user.UserRole.ToString()),
                new Claim(ClaimTypes.Email, user.Email)
            };

            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Key"]));
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(   
                issuer: configuration["JWT:Issuer"],
                audience: configuration["JWT:Audience"],
                claims: claims,
                expires: DateTime.Now.AddDays(7),
                signingCredentials: signingCredentials
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<string> Logout(string token)
        {
            User? data = default;
            try
            {
                data = context.Users.Where(u => u.RefreshToken == token).FirstOrDefault();
            }
            catch
            {
                return $"Account with token {token} could not be found.";
            }   
            #region Data Binding
            data.RefreshToken = string.Empty;
            data.TokenExpires = DateTime.UtcNow;
            data.TokenCreated = DateTime.UtcNow;
            #endregion
            context.Update(data);
            await context.SaveChangesAsync();
            return $"Logged out of {data.Email}";
        }

        public async Task<string> ChangePassword(ChangePasswordDTO request)
        {
            var data = context.Users.Where(u => u.RefreshToken == request.Token).FirstOrDefault();
            if (!Crypto.VerifyHashedPassword(data.Password,request.CurrentPassword))
            {
                throw new Exception("Password is incorrect");
            }

            return "New password set.";
        }
    }
}
