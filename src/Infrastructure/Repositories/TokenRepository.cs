﻿using Application.Repositories;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class TokenRepository(ApplicationDbContext context) : ITokenRepository
    {
        public async Task<string> InsertToken(RefreshToken token, string email)
        {
            //Awaiting data causes issues with updating
            User? data = context.Users.Where(u => u.Email.Equals(email)).First();
            #region Data Binding
            data.RefreshToken = token.Token;
            data.TokenExpires = token.Expires;
            data.TokenCreated = token.DateCreated;
            #endregion
            context.Update(data);
            await context.SaveChangesAsync();
            return token.Token;
        }
    }
}
