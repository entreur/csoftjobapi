﻿using Microsoft.AspNetCore.Http;

namespace Infrastructure.Utilities
{
    public class ImageUtilities
    {
        public static bool IsImageFile(IFormFile file)
        {
            // Limit the read to the first 512 bytes
            byte[] buffer = new byte[512];

            using (Stream? stream = file.OpenReadStream())
            {
                stream.Read(buffer, 0, buffer.Length);
            }

            // Check the magic number
            return IsJpeg(buffer) || IsPng(buffer) || IsWebp(buffer);
        }

        private static bool IsJpeg(byte[] buffer)
        {
            // JPEG magic number: FF D8 FF
            return buffer.Length >= 3 &&
                   buffer[0] == 0xFF &&
                   buffer[1] == 0xD8 &&
                   buffer[2] == 0xFF;
        }

        private static bool IsPng(byte[] buffer)
        {
            // PNG magic number: 89 50 4E 47 0D 0A 1A 0A
            return buffer.Length >= 8 &&
                   buffer[0] == 0x89 &&
                   buffer[1] == 0x50 &&
                   buffer[2] == 0x4E &&
                   buffer[3] == 0x47 &&
                   buffer[4] == 0x0D &&
                   buffer[5] == 0x0A &&
                   buffer[6] == 0x1A &&
                   buffer[7] == 0x0A;
        }

        public static bool IsWebp(byte[] buffer)
        {
            // WEBP magic number: 52 49 46 46
            return buffer.Length >= 12 &&
            buffer[0] == 0x52 &&
            buffer[1] == 0x49 &&
            buffer[2] == 0x46 &&
            buffer[3] == 0x46;
        }
    }
}
